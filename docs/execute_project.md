## init

Create a configuration file called "address_podcasts.json" and place the youtube api key
```
{
    "api_key_youtube": "put the key here"
}
```

##### Run setup
```
poetry run python setup.py install
```

#### Init database
```
poetry run np migre migrate
```


##### Run tests
```
poetry run np tests_bd run-test
```

# CRUD

##### Save a podcast category to the database
```
poetry run np crud save-model -f save-categoria
```

##### Save a podcast to the database
```
poetry run np crud save-model -f save-podcast
```

##### Search for a podcast via rss feed
```
poetry run np crud search-podcast -f search -p rss -i 'rss link here'

```

##### Search for a podcast via youtube
```
poetry run np crud search-podcast -f search -p youtube -i 'id channel youtube here'

```

##### Save podcast via youtube
```
poetry run np crud save-rss-podcast -f save-feed -p youtube -c categoty-podcast-here -i 'id channel youtube here'

```

##### Save podcast via rss
```
poetry run np crud save-rss-podcast -f save-feed -p rss -c categoty-podcast-here -i 'rss link here'

```
