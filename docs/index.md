# Welcome to Podcast notifications

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

### About

This project is being developed to send podcast notifications on the desktop

### Why not use an existing program?

Some podcasts are only available on youtube, do not have rss and are not available on desktop podcast programs.
This software is being developed to send notifications via rss or youtube
