## Project Layout


    diagrams/ # Folder to store the project diagrams files
    docs/ # Project documentation folder

    settings/
        commands/ # Command files to run the features
        migrations/ # database migrations

    src/ # folder with the functionality scripts

    tests/ # folder with tests
