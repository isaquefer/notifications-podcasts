import json

from googleapiclient.discovery import build

with open('settings/address_podcasts.json') as key:
    data = json.load(key)
    api_key = data['api_key_youtube']

service = build('youtube', 'v3', developerKey=api_key)
