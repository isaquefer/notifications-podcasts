from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///settings/pythonsqlite.db', echo=True)

Base = declarative_base(engine)
