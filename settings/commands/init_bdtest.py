from subprocess import call

from click import group


@group('tests_bd')
def tests_bd():
    ...


@tests_bd.command()
def run_test():
    command = 'coverage run --source=src -m pytest -s'.split()
    call(command)


@tests_bd.command()
def report():
    command = 'coverage report'.split()
    call(command)
