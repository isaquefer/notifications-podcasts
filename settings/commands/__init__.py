from click import group

from settings.commands.init_bdtest import tests_bd
from settings.commands.migrations import migre
from settings.commands.cruds import crud


@group('cli')
def cli():
    ...


cli.add_command(tests_bd)
cli.add_command(migre)
cli.add_command(crud)

cli()
