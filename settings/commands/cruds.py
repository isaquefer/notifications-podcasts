from subprocess import call

from click import group, option


@group('crud')
def crud():
    ...


@crud.command()
@option('-f', 'function', default='save-podcast',
              help='teste')
def save_model(function):
    command = f'poetry run python src/populate_bd.py {function}'.split()
    call(command)


@crud.command()
@option('-f', 'function', default='search')
@option('-p', 'plataforma', default='rss')
@option('-i', 'id_podcast', default='')
def search_podcast(function, plataforma, id_podcast):
    command = f'poetry run python src/search.py {function} \
-p {plataforma} -i {str(id_podcast)}'.split()
    call(command)


@crud.command()
@option('-f', 'function')
@option('-i', 'id_podcast', default='')
@option('-p', 'plataforma', default='rss', help='feed or id youtube')
@option('-c', 'categoria', default='')
def save_rss_podcast(id_podcast, plataforma, categoria, function):
    command = f'poetry run python src/save_podcast_rss.py {function} \
-p {plataforma} -i {str(id_podcast)} -c {categoria}'.split()
    call(command)


@crud.command()
@option('-f', 'function')
def notification(function):
    command = f'poetry run python src/verify_new_podcast.py {function}'.split()
    call(command)
