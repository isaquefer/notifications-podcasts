from os import sep
from subprocess import call

from click import group, option

path_ini_alembic_file = 'settings/alembic.ini'.replace('/', sep)


@group('migre')
def migre():
    ...


@migre.command()
@option('-m', 'message', default='migração via CLI',
              help='Mensagem para identificar a migrations do alembic')
def makemigration(message):
    call(
        ['alembic', '-c', path_ini_alembic_file, 'revision', '--autogenerate',
         '-m', message]
    )


@migre.command()
def migrate():
    call(['alembic', '-c', path_ini_alembic_file, 'upgrade', 'head'])
