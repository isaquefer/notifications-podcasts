from sqlalchemy import Column, String, PrimaryKeyConstraint

from settings.database import Base


class Plataforma(Base):
    __tablename__ = 'plataforma'

    nome = Column('nome', String)

    __table_args__ = (
        PrimaryKeyConstraint('nome', name='pk_plataforma'),
    )

    def __repr__(self):
        return f'<Plataforma(name="{self.nome}")>'
