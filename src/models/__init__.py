from settings.database import Base

from src.models.categoria_podcast import CategoriaPodcast
from src.models.podcast import Podcast
from src.models.plataforma import Plataforma
