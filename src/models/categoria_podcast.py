from sqlalchemy import Column, String, PrimaryKeyConstraint

from settings.database import Base


class CategoriaPodcast(Base):
    __tablename__ = 'categoria_podcast'

    nome = Column('nome', String)

    __table_args__ = (
        PrimaryKeyConstraint('nome', name='pk_categoria_podcast'),
    )

    def __repr__(self):
        return f'<CategoriaPodcast(name="{self.nome}")>'
