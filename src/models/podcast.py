from sqlalchemy import (Column, String, PrimaryKeyConstraint, Integer,
                        ForeignKeyConstraint, UniqueConstraint)
from sqlalchemy.orm import relationship

from settings.database import Base


class Podcast(Base):
    __tablename__ = 'podcast'

    id = Column('id', Integer, autoincrement=True)
    nome = Column(String, nullable=False)
    thumbnail = Column(String, nullable=False)
    url = Column(String, nullable=False)
    num_podcast = Column(Integer, nullable=False)
    titulo_ultimo_podcast = Column(String, nullable=False)
    plataforma = Column(String, nullable=False)
    categoria = Column(String, nullable=False)

    plataforma_relationship = relationship("Plataforma")
    categoria_relationship = relationship("CategoriaPodcast")

    __table_args__ = (
        PrimaryKeyConstraint(
            'id', name='pk_podcast'
        ),
        ForeignKeyConstraint(
            ['plataforma'], ['plataforma.nome'], name='fk_podcast_plataforma'
        ),
        ForeignKeyConstraint(
            ['categoria'], ['categoria_podcast.nome'],
            name='fk_podcast_plataforma'
        ),
        UniqueConstraint('nome', name='unique_podcast_nome'),
        UniqueConstraint('url', name='unique_podcast_url'),
    )

    def __repr__(self):
        return f'<Podcast(name="{self.nome}")>'
