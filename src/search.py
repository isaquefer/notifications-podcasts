import click
import feedparser

from settings.api_youtube_sevice import service


@click.group()
def cli():
    ...


@click.command()
@click.option('-i', 'id_podcast', default='')
@click.option('-p', 'plataforma', default='rss', help='feed or id youtube')
def search(id_podcast, plataforma):
    if plataforma == 'rss':
        feed = feedparser.parse(id_podcast)
        if not feed['feed']:
            print('podcast not found')
            return
        print(feed)
        return

    podcast = service.channels().list(
        part='statistics', id=str(id_podcast)
    ).execute()

    if not podcast['pageInfo']['resultsPerPage']:
        print('podcast not found')
        return
    print(podcast)


cli.add_command(search)
cli()
