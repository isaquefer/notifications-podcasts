import click
import feedparser

from settings.api_youtube_sevice import service
from src.models import Podcast
from src.controllers import Session

session = Session()


@click.group()
def cli():
    ...


def seach_rss(podcast):
    feed = feedparser.parse(podcast.url)
    if podcast.num_podcast < len(feed['entries']):
        titulo = feed['entries'][0]['title']
        print(f'Notificação\nNovo podcast: {titulo}')


def search_youtube(podcast):
    podcast_youtube = service.channels().list(
        part='snippet', id=podcast.url
    ).execute()

    count_video = service.channels().list(
        part='statistics', id=podcast.url
    ).execute()

    count_vid_int = int(count_video['items'][0]['statistics']['videoCount'])
    if podcast.num_podcast < count_vid_int:
        titulo = podcast_youtube['items'][0]['snippet']['title']
        print(f'Notificação\nNovo podcast: {titulo}')


@click.command()
def verify_podcast():
    podcasts = session.query(Podcast).all()

    for podcast in podcasts:
        if podcast.plataforma == 'rss':
            seach_rss(podcast)
        else:
            search_youtube(podcast)


cli.add_command(verify_podcast)
cli()
