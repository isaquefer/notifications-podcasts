from src.controllers import Session

from src.models.categoria_podcast import CategoriaPodcast

session = Session()


def save_categoria(categoria):
    try:
        session.add(categoria)
        session.commit()
        return 'save success'
    except Exception as e:
        error = e
        session.rollback()
        return str(error.args)
    finally:
        session.close()


def update_categoria(categoria_nome, categoria_nome_novo):
    try:
        response = session.query(CategoriaPodcast).filter(
            CategoriaPodcast.nome == categoria_nome
        ).update({'nome': categoria_nome_novo})

        if response:
            session.commit()
            return 'update success'
        return f'{categoria_nome} not found'
    except Exception:
        ...
    finally:
        session.close()


def delete_categoria(categoria_nome):
    try:
        response = session.query(CategoriaPodcast).filter(
            CategoriaPodcast.nome == categoria_nome
        ).delete()
        if response:
            session.commit()
            return 'delete success'
        return f'{categoria_nome} not found'
    except Exception:
        ...
    finally:
        session.close()
