from src.controllers import Session

from src.models.podcast import Podcast

session = Session()


def save_podcast(podcast):
    try:
        session.add(podcast)
        session.commit()
        return 'save success'
    except Exception as e:
        error = e
        session.rollback()
        return str(error.args)
    finally:
        session.close()


def update_podcast(podcast_nome, podcast_nome_novo, field):
    try:
        response = session.query(Podcast).filter(
            Podcast.nome == podcast_nome
        ).update({field: podcast_nome_novo})

        if response:
            session.commit()
            return 'update success'
        return f'{podcast_nome} not found'
    except Exception:
        ...
    finally:
        session.close()


def delete_podcast(podcast_nome):
    try:
        response = session.query(Podcast).filter(
            Podcast.nome == podcast_nome
        ).delete()
        if response:
            session.commit()
            return 'delete success'
        return f'{podcast_nome} not found'
    except Exception:
        ...
    finally:
        session.close()
