from sqlalchemy.orm import sessionmaker

from settings.database import engine

Session = sessionmaker(bind=engine)
