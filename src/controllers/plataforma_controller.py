from src.controllers import Session

from src.models.plataforma import Plataforma

session = Session()


def save_plataforma(plataforma):
    try:
        session.add(plataforma)
        session.commit()
        return 'save success'
    except Exception as e:
        error = e
        session.rollback()
        return str(error.args)
    finally:
        session.close()


def update_plataforma(plataforma_nome, plataforma_nome_novo):
    try:
        response = session.query(Plataforma).filter(
            Plataforma.nome == plataforma_nome
        ).update({'nome': plataforma_nome_novo})

        if response:
            session.commit()
            return 'update success'
        return f'{plataforma_nome} not found'
    except Exception:
        ...
    finally:
        session.close()


def delete_plataforma(plataforma_nome):
    try:
        response = session.query(Plataforma).filter(
            Plataforma.nome == plataforma_nome
        ).delete()
        if response:
            session.commit()
            return 'delete success'
        return f'{plataforma_nome} not found'
    except Exception:
        ...
    finally:
        session.close()
