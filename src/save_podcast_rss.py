import click
import feedparser

from settings.api_youtube_sevice import service
from src.models import Podcast
from src.controllers.podcast_controller import save_podcast


@click.group()
def cli():
    ...


@click.command()
@click.option('-i', 'id_podcast', default='')
@click.option('-p', 'plataforma', default='rss', help='feed or id youtube')
@click.option('-c', 'categoria', default='')
def save_feed(id_podcast, plataforma, categoria):
    if plataforma == 'rss':
        feed = feedparser.parse(id_podcast)
        podcast = Podcast(
            nome=feed['feed']['title'],
            thumbnail=feed['feed']['image']['href'],
            num_podcast=len(feed['entries']),
            titulo_ultimo_podcast=feed['entries'][0]['title'],
            plataforma=plataforma,
            categoria=categoria,
            url=id_podcast
        )
    else:
        youtube = service.channels().list(
            part='snippet', id=str(id_podcast)
        ).execute()

        count_video = service.channels().list(
            part='statistics', id=str(id_podcast)
        ).execute()

        podcast = Podcast(
            nome=youtube['items'][0]['snippet']['title'],
            thumbnail=youtube['items'][0]['snippet']['thumbnails']['default']['url'],
            num_podcast=count_video['items'][0]['statistics']['videoCount'],
            titulo_ultimo_podcast='',
            plataforma=plataforma,
            categoria=categoria,
            url=id_podcast
        )

    response = save_podcast(podcast)
    if response == 'save success':
        print('save success\n Bye!')


cli.add_command(save_feed)
cli()
