import click

from src.models import CategoriaPodcast
from src.models import Plataforma
from src.models import Podcast
from src.controllers.categoria_controller import save_categoria as sv
from src.controllers.plataforma_controller import save_plataforma as sp
from src.controllers.podcast_controller import save_podcast as spodcast


@click.group()
def cli():
    ...


@click.command()
def save_categoria():
    categoria_nome = input('Digite o nome: ')

    categoria = CategoriaPodcast(nome=categoria_nome)
    response = sv(categoria)

    if response == 'save success':
        print('save success\n Bye!')


@click.command()
def save_plataforma():
    plataforma_nome = input('Digite o nome: ')

    plataforma = Plataforma(nome=plataforma_nome)
    response = sp(plataforma)

    if response == 'save success':
        print('save success\n Bye!')


fields = {
    'nome': '',
    'thumbnail': '',
    'num_podcast': '',
    'titulo_ultimo_podcast': '',
    'plataforma': '',
    'categoria': '',
    'url': '',
}
@click.command()
def save_podcast():
    for field in fields:
        fields[field] = input(f'Digite o {field}: ')

    podcast = Podcast(**fields)
    response = spodcast(podcast)

    if response == 'save success':
        print('save success\n Bye!')


cli.add_command(save_podcast)
cli.add_command(save_plataforma)
cli.add_command(save_categoria)
cli()
