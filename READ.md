# Notifications podcasts

## Pre-requisites
- python
- poetry

### Run documentations
```
poetry install
poetry run mkdocs serve
```
