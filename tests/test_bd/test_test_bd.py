from os import sep
from os.path import exists

from sqlalchemy import inspect

from settings.database import engine
from tests.base import BaseTest

inspect = inspect(engine)


class TestDataBase(BaseTest):

    def test_exist_db_file(self):

        path = 'settings' + sep + 'pythonsqlite.db'
        if not exists(path):
            with self.assertRaises(FileExistsError):
                print('The database was not created')

    def test_exist_tables(self):

        list_tables = ['podcast', 'plataforma', 'categoria_podcast']
        tables_bd = [tables_ins for tables_ins in inspect.get_table_names()]
        table_exist = [
            tables for tables in list_tables if tables not in tables_bd
        ]

        msg = ', '.join(table for table in table_exist)
        if table_exist:
            with self.assertRaises(Exception):
                print(f'The tables: {msg} was not created')
