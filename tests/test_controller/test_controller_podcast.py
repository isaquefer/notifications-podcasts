from tests.base import BaseTest
from src.models.podcast import Podcast
from src.controllers.podcast_controller import (save_podcast,
                                                update_podcast,
                                                delete_podcast)


class TestDataBase(BaseTest):

    def test_01_save_podcast(self):
        podcast = Podcast(
            nome='teste', thumbnail='teste', num_podcast=5,
            titulo_ultimo_podcast='teste', plataforma='rss', url='teste',
            categoria='catolicismo'
        )
        msg = save_podcast(podcast)
        self.assertEqual(msg, 'save success')

    def test_02_save_podcast_with_same_name(self):
        podcast = Podcast(
            nome='teste', thumbnail='teste', num_podcast=5,
            titulo_ultimo_podcast='teste', plataforma='rss', url='testes',
            categoria='catolicismo'
        )

        msg = save_podcast(podcast)
        self.assertEqual(
            msg,
            "('(sqlite3.IntegrityError) UNIQUE constraint failed: \
podcast.nome',)"
        )

    def test_03_save_podcast_with_nullable(self):
        podcast = Podcast(
            nome='teste1', thumbnail='teste', num_podcast=5,
            plataforma='rss', categoria='catolicismo', url='teste'
        )

        msg = save_podcast(podcast)
        self.assertEqual(
            msg,
            "('(sqlite3.IntegrityError) NOT NULL constraint failed: \
podcast.titulo_ultimo_podcast',)"
        )

    def test_03_update_podcast_not_found(self):
        msg = update_podcast('testesss', 'teste_dois', 'nome')
        self.assertEqual(msg, 'testesss not found')

    def test_04_update_podcast(self):
        msg = update_podcast('teste', 'teste_dois', 'nome')
        self.assertEqual(msg, 'update success')

    def test_05_update_podcast(self):
        msg = update_podcast('teste_dois', 'thumb', 'thumbnail')
        self.assertEqual(msg, 'update success')

    def test_05_delete_podcast_not_found(self):
        msg = delete_podcast('testesss')
        self.assertEqual(msg, 'testesss not found')

    def test_06_delete_podcast(self):
        msg = delete_podcast('teste_dois')
        self.assertEqual(msg, 'delete success')
