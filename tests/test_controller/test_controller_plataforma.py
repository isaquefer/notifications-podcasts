from tests.base import BaseTest
from src.models.plataforma import Plataforma
from src.controllers.plataforma_controller import (save_plataforma,
                                                   update_plataforma,
                                                   delete_plataforma)


class TestDataBase(BaseTest):

    def test_01_save_plataforma(self):
        plataforma = Plataforma(nome='teste')
        msg = save_plataforma(plataforma)
        self.assertEqual(msg, 'save success')

    def test_02_save_plataforma_with_same_name(self):
        plataforma = Plataforma(nome='teste')
        msg = save_plataforma(plataforma)
        self.assertEqual(
            msg,
            "('(sqlite3.IntegrityError) UNIQUE constraint failed: \
plataforma.nome',)"
        )

    def test_03_update_plataforma_not_found(self):
        msg = update_plataforma('testesss', 'teste_dois')
        self.assertEqual(msg, 'testesss not found')

    def test_04_update_plataforma(self):
        msg = update_plataforma('teste', 'teste_dois')
        self.assertEqual(msg, 'update success')

    def test_05_delete_plataforma_not_found(self):
        msg = delete_plataforma('testesss')
        self.assertEqual(msg, 'testesss not found')

    def test_06_delete_plataforma(self):
        msg = delete_plataforma('teste_dois')
        self.assertEqual(msg, 'delete success')
