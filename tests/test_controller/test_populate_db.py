from tests.base import BaseTest
from src.models.categoria_podcast import CategoriaPodcast
from src.controllers.categoria_controller import (save_categoria,
                                                  update_categoria,
                                                  delete_categoria)


class TestDataBase(BaseTest):

    def test_01_save_categoria(self):
        categoria = CategoriaPodcast(nome='teste')
        msg = save_categoria(categoria)
        self.assertEqual(msg, 'save success')

    def test_02_save_categoria_with_same_name(self):
        categoria = CategoriaPodcast(nome='teste')
        msg = save_categoria(categoria)
        self.assertEqual(
            msg,
            "('(sqlite3.IntegrityError) UNIQUE constraint failed: \
categoria_podcast.nome',)"
        )

    def test_03_update_categoria_not_found(self):
        msg = update_categoria('testesss', 'teste_dois')
        self.assertEqual(msg, 'testesss not found')

    def test_04_update_categoria(self):
        msg = update_categoria('teste', 'teste_dois')
        self.assertEqual(msg, 'update success')

    def test_05_delete_categoria_not_found(self):
        msg = delete_categoria('testesss')
        self.assertEqual(msg, 'testesss not found')

    def test_06_delete_categoria(self):
        msg = delete_categoria('teste_dois')
        self.assertEqual(msg, 'delete success')
