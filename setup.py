from os import sep

from setuptools import setup, find_packages


setup(
    name='np',
    version='0.0.1',
    packages=find_packages(),
    install_requires=['Click'],
    entry_points={
        'console_scripts': ['np = settings.commands:cli'],
    }
)
